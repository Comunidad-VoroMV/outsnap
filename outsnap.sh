#!/bin/bash
echo
echo "Bievenido al desinstalador de paquetería snap para Ubuntu"
echo
sleep 1s
echo
echo "Listando paquetes snap instalados en el sistema..."
sleep 3s
echo
snap list
echo
echo "Deteniendo servicios de snapd"
sleep 1s
echo
sudo systemctl disable snapd.service
sudo systemctl disable snapd.socket
sudo systemctl disable snapd.seeded.service
echo
echo "Desinstalando snaps..."
sleep 1s
echo
sudo snap remove firefox
sudo snap remove snap-store
sudo snap remove gtk-common-themes
sudo snap remove gnome-3-38-2004
sudo snap remove snapd-desktop-integration
sudo snap remove core20
sudo snap remove bare
sudo snap remove snapd
echo
echo "Eliminamos la cache de snap"
sleep 1s
echo
sudo rm -rf /var/cache/snapd/
echo
echo "Desinstalando snap"
echo
sudo apt autoremove --purge -y snapd
echo
echo "Eliminamos directorios de snap del sistema"
rm -rf ~/snap
sudo rm -rf /snap
sudo rm -rf /var/snap
sudo rm -rf /var/lib/snapd
echo
echo "Modificando preferencias del gestor de paquetes"
sleep 1s
echo
sudo cp nosnap.pref /etc/apt/preferences.d ; sudo apt update
echo
echo "Instalando de Firefox en formato DEB convencional"
echo
sudo touch /etc/apt/preferences.d/mozillateamppa
echo "Package: firefox*" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
echo "Pin: release o=LP-PPA-mozillateam" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
echo "Pin-Priority: 501" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
sudo add-apt-repository ppa:mozillateam/ppa
sudo apt update -y ; sudo apt install -y firefox
echo
echo "Instalando el centro de software de GNOME"
sleep 1s
echo
sudo apt install -y gnome-software
echo
echo "¡Hecho! Porfavor reinicia el sistema"
echo
