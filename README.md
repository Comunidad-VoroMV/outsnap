# SNAPINSTALL

Esto es un script sencillo para limpiar Ubuntu de paquetes snap e impedir su uso forzado de estos paquetes.


# FUNCIONAMIENTO/INSTALACIÓN:

El script se ejecuta con permisos de ejecución en una terminal y el resto sera indicar la distribución que tienes, te indicaré las ordenes necesarias que tienes que ejecutar.


```bash
chmod +x outsnap.sh    #para dar permisos de ejecución al script
```

```bash
sudo ./outsnap.sh     #Para ejecutar el script
```
